﻿using System;

namespace practice
{
    interface IHeap<T>
    {
        void Push(T data);
        T Pop();
        T GetMin();
    }

    public class Heap<T> : IHeap<T> where T : IComparable<T>
    {

        private readonly LocalLinkedList<T> _heapArray;


        public Heap()
        {
            _heapArray = new LocalLinkedList<T> { };
        }

        private int HeapSize()
        {
            return _heapArray.Lenght;
        }

        private T Value(int index)
        {
            return _heapArray[index];
        }

        private static int LeftIndex(int index)
        {
            return 2 * index + 1;
        }

        private static int ParentIndex(int index)
        {
            return (index - 1) / 2;
        }

        private static int RightIndex(int index)
        {
            return 2 * index + 2;
        }

        private void SiftUp(int index)
        {
            while (index > 0 && Value(index).CompareTo(Value(ParentIndex(index))) > 0)
            {
                Swap(index, ParentIndex(index));
                index = ParentIndex(index);
            }
        }

        private void Swap(int firstIndex, int secondIndex)
        {
            var temp = _heapArray[firstIndex];
            _heapArray[firstIndex] = _heapArray[secondIndex];
            _heapArray[secondIndex] = temp;
        }

        private void Heapify(int index)
        {
            int left = LeftIndex(index);
            int right = RightIndex(index);
            int max = index;

            if (left >= HeapSize() && right >= HeapSize())
            {
                return;
            }

            if (left < HeapSize())
            {
                if (Convert.ToInt32(Value(left).CompareTo(Value(max))) > 0)
                {
                    max = left;
                }
            }
            if (right < HeapSize())
            {
                if (Convert.ToInt32(Value(right).CompareTo(Value(max))) > 0)
                {
                    max = right;
                }
            }

            if (max != index)
            {
                Swap(max, index);
                Heapify(max);
            }
        }

        public void Push(T value)
        {
            _heapArray.Add(value);
            SiftUp(HeapSize() - 1);
        }

        public T Pop()
        {
            var max = _heapArray[0];
            _heapArray[0] = _heapArray[HeapSize() - 1];
            _heapArray.RemoveAt(HeapSize() - 1);
            Heapify(0);
            return max;
        }

        public T GetMin()
        {
            return _heapArray[0];
        }
    }
}
