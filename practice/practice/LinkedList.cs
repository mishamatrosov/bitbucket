﻿using System;

namespace practice
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }

        public T Data { get; set; }

        public Node<T> Previous { get; set; }

        public Node<T> Next { get; set; }
    }

    public class LocalLinkedList<T>
    {
        private Node<T> _head;
        private Node<T> _tail;
        private int _size;

        public int Lenght => _size;

        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);

            if (_head == null)
            {
                _head = node;
            }
            else
            {
                _tail.Next = node;
                node.Previous = _tail;
            }
            _tail = node;
            _size++;
        }

        public void AddFirst(T data)
        {
            Node<T> node = new Node<T>(data);

            if (_head == null)
            {
                _head = node;
            }
            else
            {
                node.Next = _head;
                _head.Previous = node;
                _head = node;
            }
            _size++;
        }

        public T this[int index]
        {

            get
            {
                Node<T> temp = _head;
                int i = 0;
                while (index != i)
                {
                    temp = temp.Next;
                    i++;
                }
                return temp.Data;
            }
            set
            {
                Node<T> temp = _head;
                int i = 0;
                while (index != i)
                {
                    temp = temp.Next;
                    i++;
                }
                temp.Data = value;
            }
        }

        public bool IsEmpty()
        {
            return _size == 0;
        }

        public void Insert(T data, int index)
        {
            Node<T> node = new Node<T>(data);
            Node<T> temp = _head;

            if (index == 0)
            {
                AddFirst(data);
                return;
            }

            if (index >= _size)
            {
                throw new Exception("Invalid index!");
            }

            int count = 0;
            while (count != index - 1)
            {
                temp = temp.Next;
                count++;
            }
            node.Next = temp.Next;
            temp.Next.Previous = node;
            temp.Next = node;
            node.Previous = temp;
            _size++;
        }

        private void RemoveFirst()
        {
            _head = _head.Next;
            if (_head != null)
            {
                _head.Previous = null;
            }
            _size--;
            GC.Collect();
        }

        private void RemoveLast()
        {
            _tail = _tail.Previous;
            _tail.Next = null;
            _size--;
            GC.Collect();
        }

        public void RemoveAt(int index)
        {
            if (index == 0)
            {
                RemoveFirst();
                return;
            }

            if (index == _size - 1)
            {
                RemoveLast();
                return;
            }

            int i = 0;
            Node<T> temp = _head;

            while (i < index)
            {
                temp = temp.Next;
                i++;
            }

            temp.Previous.Next = temp.Next;
            temp.Next.Previous = temp.Previous;
            _size--;

            GC.Collect();
        }

        public void Clear()
        {
            int size = _size;

            for (int i = 0; i < size; i++)
            {
                RemoveFirst();
            }
        }
    }
}
