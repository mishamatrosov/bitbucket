﻿using System;

namespace practice
{
    public class PriorityQueue<T> where T : IComparable<T>
    {
        private readonly Heap<T> _priorityQueue;

        public PriorityQueue()
        {
            _priorityQueue = new Heap<T>();
        }

        public void Enqueue(T value)
        {
            _priorityQueue.Push(value);
        }

        public T Dequeue()
        {
            return _priorityQueue.Pop();
        }

        public T Peek()
        {
            return _priorityQueue.GetMin();
        }

    }
}